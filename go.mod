module gitlab.com/telecom-tower/sdk

require (
	github.com/pkg/errors v0.8.1
	gitlab.com/telecom-tower/api-go v1.3.1
	google.golang.org/grpc v1.23.0
)
